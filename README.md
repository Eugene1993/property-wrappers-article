Привет, Хабр! Представляю вашему вниманию перевод статьи [Swift Property Wrappers](https://forums.swift.org/t/pitch-property-delegates/21895 "Swift Property Wrappers") автора [Mattt](https://nshipster.com/authors/mattt/ "Mattt").

https://nshipster.com/propertywrapper/#changing-synthesized-equality-and-comparison-semantics

Property wrappers были впервые представлены на форумах [Swift](https://forums.swift.org/t/pitch-property-delegates/21895 "Swift") еще в марте 2019 года, за несколько месяцев до объявления SwiftUI. В своем первоначальном предложении член Swift Core Team Douglas Gregor описал эту фичу (тогда она называлась «property delegates») как доступное пользователю обобщение функциональности, в настоящее время предоставляемой такими языковыми конструкциями, как, например, `lazy`.

Если свойство объявлено с ключевым словом `lazy`, это значит, что оно будет инициализировано при первом обращении к нему. Например, отложенную инициализацию свойства можно было бы реализовать с помощью закрытого свойства, доступ к которому осуществляется через вычисляемое свойство. Но с помощью ключевого слова `lazy` это сделать гораздо легче.

```swift
struct Structure {
    // Отложенная инициализация свойства с помощью lazy
    lazy var deferred = …

    // Аналогичная реализация с помощью закрытого и вычисляемого свойства
    private var _deferred: Type?
    var deferred: Type {
        get {
            if let value = _deferred { return value }
            let initialValue = …
            _deferred = initialValue
            return initialValue
        }

        set {
            _deferred = newValue
        }
    }
}
```

В [SE-0258: Property Wrapper](https://github.com/apple/swift-evolution/blob/master/proposals/0258-property-wrappers.md "SE-0258: Property Wrappers") отлично объясняется дизайн и реализация Property Wrappers. Поэтому вместо того, чтобы пытаться улучшить описание в официальной документации, рассмотрим несколько примеров, которые можно реализовать с помощью Property Wrappers:

- Ограничение значений свойств
- Преобразование значений при изменении свойств
- Изменение семантики равенства и сравнения свойств
- Логирование доступа к свойству

### Ограничение значений свойств

[SE-0258: Property Wrapper](https://github.com/apple/swift-evolution/blob/master/proposals/0258-property-wrappers.md "SE-0258") приносит несколько практических примеров, включаяя `@Clamping`, `@Copying`, `@Atomic`, `@ThreadSpecific`, `@Box`, `@UserDefault`. Рассмотрим `@Clamping` Property Wrapper, которое позволяет ограничить максимальное/минимальное значение свойства. 

```swift
@propertyWrapper
struct Clamping<Value: Comparable> {
    var value: Value
    let range: ClosedRange<Value>

    init(initialValue value: Value, _ range: ClosedRange<Value>) {
        precondition(range.contains(value))
        self.value = value
        self.range = range
    }

    var wrappedValue: Value {
        get { value }
        set { value = min(max(range.lowerBound, newValue), range.upperBound) }
    }
}
```

`@Clamping` можно использовать, например, для моделирования кислотности раствора, величина которой может принимать значение от 0 до 14.

```swift
struct Solution {
    @Clamping(0...14) var pH: Double = 7.0
}

let carbonicAcid = Solution(pH: 4.68)
```

Попытка установить значение pH, выходящее за диапазон от `(0...14)` приведёт к тому, что свойство примет ближайшее к минимуму или максимуму интервала значение  

```swift
let superDuperAcid = Solution(pH: -1)
superDuperAcid.pH // 0
```

Property wrappers могут использоваться при реализации других Property wrappers. Например `@UnitInterval` Property wrapper ограничивает значение свойства интервалом `(0...1)` используя `@Clamping(0...1)`

```swift
@propertyWrapper
struct UnitInterval<Value: FloatingPoint> {
    @Clamping(0...1)
    var wrappedValue: Value = .zero

    init(initialValue value: Value) {
        self.wrappedValue = value
    }
}
```

## Похожие идеи

- `@Positive` / `@NonNegative`, указывающее, что значение может быть либо положительным, либо отрицательным числом
- `@NonZero`, указывающее, что значение свойства не может быть равно 0
- `@Validated` или `@Whitelisted` / `@Blacklisted`, ограничивающее значение свойства определенными значениями 

### Преобразование значений при изменении свойств

Валидация значений текстовых полей это постоянная головная боль разработчиков приложений. Существует очень много вещей, которые нужно отслеживать: от банальностей типа кодировки до злонамеренных попыток ввести код через текстовое поле. Рассмотрим применение Property Wrappers для удаления пробелов в начале и в конце строки, которую ввёл пользователь. 

```swift
import Foundation

let url = URL(string: " https://habrahabr.ru") // nil

let date = ISO8601DateFormatter().date(from: " 2019-06-24") // nil

let words = " Hello, world!".components(separatedBy: .whitespaces)
words.count // 3
```

`Foundation` предлагает метод `trimmingCharacters(in:)`, с помощью которого можно удалить пробелы в начале и в конце строки. Можно вызывать этот метод всегда, когда нужно гарантировать правильность ввода, но это не очень удобно. Для этого можно использовать Property Wrappers.

```swift
import Foundation

@propertyWrapper
struct Trimmed {
    private(set) var value: String = ""

    var wrappedValue: String {
        get { value }
        set { value = newValue.trimmingCharacters(in: .whitespacesAndNewlines) }
    }

    init(initialValue: String) {
        self.wrappedValue = initialValue
    }
}
```

Помечая каждое свойство атрибутом `@Trimmed` любое строковое значение, присвоенное свойствам `title` или `body`

```swift
struct Post {
    @Trimmed var title: String
    @Trimmed var body: String
}

let quine = Post(title: "  Swift Property Wrappers  ", body: "…")
quine.title // "Swift Property Wrappers" - без пробелов в начале и в конце

quine.title = "      @propertyWrapper     "
quine.title // "@propertyWrapper"
```

## Похожие идеи

- `@Transformed` которое применяет [ICU-преобразование](https://developer.apple.com/documentation/foundation/nsstring/1407787-applyingtransform "ICU-преобразование") к введённой строке
- `@Rounded` / `@Truncated` которое округляет\урезает значение строки

### Изменение семантики равенства и сравнения свойств

В Swift две строки равны, если они [канонично эквивалентны](https://unicode.org/reports/tr15/#Canon_Compat_Equivalence "канонично эквивалентны"): если строки содержит одинаковые символы, то они равны. Но допустим, мы хотим, чтобы строковые свойства были равны без учета регистра символов, которые они содержат.

`@CaseInsensitive` реализует оболочку для свойств, имеющих тип `String` или `SubString`.

```swift
import Foundation

@propertyWrapper
struct CaseInsensitive<Value: StringProtocol> {
    var wrappedValue: Value
}

extension CaseInsensitive: Comparable {
    private func compare(_ other: CaseInsensitive) -> ComparisonResult {
        wrappedValue.caseInsensitiveCompare(other.wrappedValue)
    }

    static func == (lhs: CaseInsensitive, rhs: CaseInsensitive) -> Bool {
        lhs.compare(rhs) == .orderedSame
    }

    static func < (lhs: CaseInsensitive, rhs: CaseInsensitive) -> Bool {
        lhs.compare(rhs) == .orderedAscending
    }

    static func > (lhs: CaseInsensitive, rhs: CaseInsensitive) -> Bool {
        lhs.compare(rhs) == .orderedDescending
    }
}
```

```swift
let hello: String = "hello"
let HELLO: String = "HELLO"

hello == HELLO // false
CaseInsensitive(wrappedValue: hello) == CaseInsensitive(wrappedValue: HELLO) // true
```

## Похожие идеи

- `@Approximate` для приблизительного сравнения свойств, имеющих тип Double или Float
- `@Ranked` для свойств, значения которых имеют порядок (например, ранг игральных карт)

### Логирование доступа к свойству

`@Versioned` позволит перехватывать присвоенные значения и запоминать, когда они были установлены

```swift
import Foundation

@propertyWrapper
struct Versioned<Value> {
    private var value: Value
    private(set) var timestampedValues: [(Date, Value)] = []

    var wrappedValue: Value {
        get { value }

        set {
            defer { timestampedValues.append((Date(), value)) }
            value = newValue
        }
    }

    init(initialValue value: Value) {
        self.wrappedValue = value
    }
}
```

Класс `ExpenseReport` позволяет сохранить временные метки состояний обработки отчета о расходах.

```swift
class ExpenseReport {
    enum State { case submitted, received, approved, denied }

    @Versioned var state: State = .submitted
}
```

Но пример выше демонстрирует серьезное ограничение в текущей реализации Property Wrappers, которое вытекает из ограничения Swift: свойства не могут генерировать исключения. Если бы мы хотели добавить в `@Versioned` ограничение для предотвращения изменения значения на `.approved` после того, как оно приняло значения `.denied`, то наилучший вариант - `fatalError()`, который плохо подходит для реальных приложений.

```swift
class ExpenseReport {
    @Versioned var state: State = .submitted {
        willSet {
            if newValue == .approved,
                $state.timestampedValues.map { $0.1 }.contains(.denied)
            {
                fatalError("Ошибка")
            }
        }
    }
}

var tripExpenses = ExpenseReport()
tripExpenses.state = .denied
tripExpenses.state = .approved // Fatal error: "Ошибка" и крэш приложения
```

## Похожие идеи

- `@Audited` для логирования доступа к свойству
- `@UserDefault` для инкапсулирования механизма чтения и сохранения данных в `UserDefaults`

### Ограничения

## Свойства не могут генерировать исключения

Как уже было сказано выше, Property Wrappers могут использовать лишь несколько методов обработки недопустимых значений

- Игнорировать их
- Завершение работы приложения при помощи fatalError()

## Свойства, имеющие Property Wrappers, не могут быть помечены атрибутом `typealias`

Пример `@UnitInterval` выше, свойство которого ограничено интервалом `(0...1)` не может быть объявлен как

```swift
typealias UnitInterval = Clamping(0...1) // Нельзя
```

## Ограничение на использование композиции из нескольких Property Wrappers

Композиция Property Wrappers не коммутативная операция: на поведение будет влиять порядок объявления. Рассмотрим пример, в котором свойство slug представляющее собой url поста в блоге, нормализуется, и результат нормализации будет различаться в зависимости от того, когда пробелы будут заменены тире: до или после того, как будут удалены пробелы. Поэтому на данный момент композиция из нескольких Property Wrappers не поддерживается.

```swift
@propertyWrapper
struct Dasherized {
    private(set) var value: String = ""

    var wrappedValue: String {
        get { value }
        set { value = newValue.replacingOccurrences(of: " ", with: "-") }
    }

    init(initialValue: String) {
        self.wrappedValue = initialValue
    }
}

struct Post {
    …
    @Dasherized @Trimmed var slug: String // error: multiple property wrappers are not supported
}
```

Однако это ограничение можно обойти, если использовать вложенные Property Wrappers

```swift
@propertyWrapper
struct TrimmedAndDasherized {
    @Dasherized
    private(set) var value: String = ""

    var wrappedValue: String {
        get { value }
        set { value = newValue.trimmingCharacters(in: .whitespacesAndNewlines) }
    }

    init(initialValue: String) {
        self.wrappedValue = initialValue
    }
}

struct Post {
    …
    @TrimmedAndDasherized var slug: String
}
```

## Другие ограничения Property Wrappers

- Нельзя использовать внутри протокола.
- Экземпляр свойства с Property Wrapper не может быть объявлен в `enum`.
- Свойство с Property Wrapper, объявленное внутри класса, не может быть переопределено другим свойством.
- Свойство с Property Wrapper не может быть `lazy`, `@NSCopying`, `@NSManaged`, `weak` или `unowned`.
- Свойство с Property Wrapper должно быть единственным в рамках своего определения (т.е. нельзя `@Lazy var (x, y) = /* ... */` ).
- У свойство с Property Wrapper нельзя определить `getter` и `setter`.
- Типы у свойства `wrappedValue` и у переменной `wrappedValue` в `init(wrappedValue:)` должны иметь тот же уровень доступа, что и тип Propery Wrapper'а.
- Тип свойство `projectedValue` должен иметь тот же уровень доступа, что и тип Propery Wrapper'а.
- `init()` должен иметь тот же уровень доступа, что и тип Propery Wrapper'а.

Давайте подытожим: Property Wrappers в Swift предоставляют авторам библиотек доступ к высокоуровневому поведению ранее зарезервированному для языковых функций. Их потенциал для улучшения читаемости и уменьшения сложности кода огромен, и мы только поверхностно рассмотрели возможности этого инструмента.

*Используете ли вы Property Wrappers в своих проектах? Пишите в комментариях!*